package it.lundstedt.erik.clipboard;

public class Config
{
String cfgDir="";

public Config(String cfgDir) {
	this.cfgDir = cfgDir;
}

public String getCfgDir() {
	return cfgDir;
}

public void setCfgDir(String cfgDir) {
	this.cfgDir = cfgDir;
}
}
